﻿using OdysseyAPI.Models;
using OdysseyAPI.Services;
using System;
using System.Collections.Generic;

namespace OdysseyAPI.Logic
{
    public class UserLeaveLogic
    {
        private readonly OdysseyService _odysseyService;

        public UserLeaveLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public List<UserLeave> GetAllLeaveForUser(string userEmailAdress)
        {
            return _odysseyService.GetAllLeaveForUser(userEmailAdress);
        }

        public UserLeave GetSpecificUserLeave(string userLeaveId)
        {
            return _odysseyService.GetSpecificUserLeave(userLeaveId);
        }

        public UserLeave UpdateUserLeave(UserLeave userLeave)
        {
            return _odysseyService.UpdateUserLeave(userLeave);
        }

        public bool CreateNewUserLeave(UserLeave userLeave)
        {
            var TotalWorkDays = CalculateLeaveForUser(userLeave);

            var LatestUserLeave = GetLatestUserLeave(userLeave);

            var leaveDaysLeft = LatestUserLeave.Remaining_Leave - TotalWorkDays;

            if (leaveDaysLeft > 0)
            {
                userLeave.Remaining_Leave = leaveDaysLeft;
                userLeave.Leave_Taken = TotalWorkDays;
                userLeave.ModifiedDate = DateTime.Now;
                userLeave.IsPending = true;

                if (_odysseyService.CreateNewUserLeave(userLeave))
                {
                    return true;
                }
            }
            return false;
        }

        private UserLeave GetLatestUserLeave(UserLeave userLeave)
        {
            return _odysseyService.GetLatestUserLeaveForUser(userLeave);
        }

        private double CalculateLeaveForUser(UserLeave userLeave)
        {
            var TotalDays = (userLeave.EndDate - userLeave.StartDate).TotalDays;

            int loop = 0;
            double TotalWorkDays = 0;

            while (loop <= TotalDays)
            {
                var GetDate = userLeave.StartDate.AddDays(loop);

                if (IsDayOfWeek(GetDate))
                {
                    if (IsDayNotPublicHoliday(GetDate))
                    {
                        TotalWorkDays++;
                    }
                }

                loop++;
            }

            return TotalWorkDays;
        }

        private bool IsDayOfWeek(DateTime date)
        {
            bool result = true;
            var day = (date.DayOfWeek).ToString();

            if (day == OdysseyLookUp.WeekendDayCode.Saterday || day == OdysseyLookUp.WeekendDayCode.Sunday)
                result = false;

            return result;
        }

        private bool IsDayNotPublicHoliday(DateTime date)
        {
            List<Holiday> GetHolidays = _odysseyService.GetAllHolidays();

            foreach (var holiday in GetHolidays)
            {
                if (holiday.DateOfHoliday.Date == date.Date)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
