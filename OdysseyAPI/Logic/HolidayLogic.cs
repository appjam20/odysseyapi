﻿using OdysseyAPI.Models;
using OdysseyAPI.Services;
using System.Collections.Generic;

namespace OdysseyAPI.Logic
{
    public class HolidayLogic
    {
        private readonly OdysseyService _odysseyService;

        public HolidayLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public List<Holiday> GetHolidays()
        {
            return _odysseyService.GetAllHolidays();
        }
    }
}
