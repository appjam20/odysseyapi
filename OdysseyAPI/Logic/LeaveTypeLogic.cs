﻿using OdysseyAPI.Models;
using OdysseyAPI.Services;
using System.Collections.Generic;

namespace OdysseyAPI.Logic
{
    public class LeaveTypeLogic
    {
        private readonly OdysseyService _odysseyService;

        public LeaveTypeLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public List<LeaveType> GetAllLeaveTypes()
        {
            return _odysseyService.GetAllLeaveTypes();
        }

        public LeaveType GetSpecificLeaveType(string leaveId)
        {
            return _odysseyService.GetSpecificLeaveType(leaveId);
        }
    }
}
