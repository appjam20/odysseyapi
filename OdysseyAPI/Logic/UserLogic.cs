﻿using OdysseyAPI.Models;
using OdysseyAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdysseyAPI.Logic
{
    public class UserLogic
    {

        private readonly OdysseyService _odysseyService;

        public UserLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public bool CheckForExistingUser(string emailAddress)
        {
            return _odysseyService.CheckForExistingUser(emailAddress);
        }

        public User GetUser(User user)
        {
            return _odysseyService.GetUser(user);
        }

        public User CreateNewUser(User user)
        {
            return _odysseyService.CreateNewUser(user);
        }

        public User UpdateUser(User user)
        {
            return _odysseyService.UpdateUser(user);
        }

        public bool DeleteUser(User user)
        {
            return _odysseyService.DeleteUser(user);
        }

    }
}
