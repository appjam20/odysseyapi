﻿using Microsoft.AspNetCore.Mvc;
using OdysseyAPI.Logic;
using OdysseyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdysseyAPI.Controllers
{
    public class UserLeaveController : ControllerBase
    {
        private readonly UserLeaveLogic _userLeaveLogic;

        public UserLeaveController(UserLeaveLogic userLeaveLogic)
        {
            _userLeaveLogic = userLeaveLogic;
        }

        [Route("GetAllLeaveForUser")]
        [HttpGet]
        public List<UserLeave> GetAllLeaveForUser(string userEmailAdress)
        {
            return _userLeaveLogic.GetAllLeaveForUser(userEmailAdress);
        }

        [Route("UpdateUserLeave")]
        [HttpPut]
        public UserLeave UpdateUserLeave(UserLeave userLeave)
        {
            return _userLeaveLogic.UpdateUserLeave(userLeave);
        }

        [Route("CreateNewUserLeave")]
        [HttpPost]
        public bool CreateNewUserLeave(UserLeave userLeave)
        {
            return _userLeaveLogic.CreateNewUserLeave(userLeave);
        }

        [Route("GetSpecificUserLeave")]
        [HttpGet]
        public UserLeave GetSpecificUserLeave(string userLeaveId)
        {
            return _userLeaveLogic.GetSpecificUserLeave(userLeaveId);
        }
    }
}
