﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OdysseyAPI.Logic;
using OdysseyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdysseyAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly UserLogic _userLogic;

        public UserController(UserLogic userService)
        {
            _userLogic = userService;
        }

        [Route("CheckUser")]
        [HttpGet]
        public bool CheckUser(string emailAddress)
        {
            return _userLogic.CheckForExistingUser(emailAddress);
        }

        [Route("GetUser")]
        [HttpGet]
        public User GetUser(User user)
        {
            return _userLogic.GetUser(user);
        }

        [Route("CreateUser")]
        [HttpPost]
        public User CreateUser(User user)
        {
            return _userLogic.CreateNewUser(user);
        }

        [Route("UpdateUser")]
        [HttpPut]
        public User UpdateUser(User user)
        {
            return _userLogic.UpdateUser(user);
        }

        [Route("DeleteUser")]
        [HttpDelete]
        public bool DeleteUser(User user)
        {
            return _userLogic.DeleteUser(user);
        }
    }
}
