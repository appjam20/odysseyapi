﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OdysseyAPI.Logic;
using OdysseyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdysseyAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LeaveTypeController : ControllerBase
    {
        private readonly LeaveTypeLogic _leaveTypeLogic;

        public LeaveTypeController(LeaveTypeLogic leaveTypeLogic)
        {
            _leaveTypeLogic = leaveTypeLogic;
        }

        [Route("GetAllLeaveTypes")]
        [HttpGet]
        public List<LeaveType> GetAllLeaveTypes()
        {
            return _leaveTypeLogic.GetAllLeaveTypes();
        }

        [Route("GetSpecificLeaveType")]
        [HttpGet]
        public LeaveType GetSpecificLeaveType(string leaveTypeId)
        {
            return _leaveTypeLogic.GetSpecificLeaveType(leaveTypeId);
        }
    }
}
