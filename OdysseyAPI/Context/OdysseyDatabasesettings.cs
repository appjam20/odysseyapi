﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdysseyAPI.Context
{
    public class OdysseyDatabasesettings : IOdysseyDatabasesettings
    {
        public string UsersCollectionName { get; set; }
        public string LeaveTypeCollectionName { get; set; }
        public string UserLeaveCollectionName { get; set; }
        public string HolidayCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IOdysseyDatabasesettings
    {
        public string UsersCollectionName { get; set; }
        public string LeaveTypeCollectionName { get; set; }
        public string UserLeaveCollectionName { get; set; }
        public string HolidayCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
